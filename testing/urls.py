"""tsoi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from testing.views import get_test, get_results, CustomUserViewSet, get_history, get_all_history

urlpatterns = [
    path('get_test/<int:pk>/', get_test, name='get test'),
    path('get_result/<int:pk>/', get_results, name='get res'),
    path('users/', CustomUserViewSet.as_view()),
    path('get_history/', get_history, name='get hist'),
    path('get_history_admin/', get_all_history, name='get all hist'),

]

