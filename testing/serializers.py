from django.contrib.auth.hashers import make_password
from django.db import IntegrityError
from rest_framework import serializers
from rest_framework.exceptions import ParseError, APIException
import json
from testing.models import User, Test, Question, Result


class CustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password')

    def create(self, validated_data):
        fields = set(self.Meta.fields) - set(validated_data.keys())
        if fields:
            raise ParseError('Отсутствуют поля - ' + str(fields))
        validated_data['password'] = make_password(validated_data['password'])
        validated_data.update({'username': validated_data['email']})
        inst = User(**validated_data)
        try:
            inst.save()
        except IntegrityError:
            raise APIException('Такой пользователь уже существует!')
        return inst





class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ['password']



class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('number', 'text')


class ResultSerializer(serializers.ModelSerializer):
    json_res = serializers.JSONField()
    class Meta:
        model = Result
        fields = ['json_res']




class AllResultSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    class Meta:
        model = Result
        fields = ['username', 'result', 'datetime']

    def get_username(self, obj):
        user = User.objects.get(id=obj.user_id)
        return user.username



class TestSerializer(serializers.ModelSerializer):

    questions = serializers.SerializerMethodField()

    class Meta:
        model = Test
        fields = ('id', 'name', 'questions')

    def get_questions(self, obj):
        inst = Question.objects.filter(test_id=obj.id)
        serializer = QuestionSerializer
        return serializer(instance=inst, many=True).data




