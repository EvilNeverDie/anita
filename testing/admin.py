from django.contrib import admin

from testing.models import Test, Question, Answer, Character, Result, User

@admin.register(User)
class CUserAdmin(admin.ModelAdmin):
    pass


@admin.register(Result)
class ActivityAdmin(admin.ModelAdmin):
    pass


@admin.register(Test)
class ActivityAdmin(admin.ModelAdmin):
    pass


@admin.register(Question)
class EventDayAdmin(admin.ModelAdmin):
    pass


@admin.register(Answer)
class ActivityAdmin(admin.ModelAdmin):
    pass


@admin.register(Character)
class EventDayAdmin(admin.ModelAdmin):
    pass


# Register your models here.
