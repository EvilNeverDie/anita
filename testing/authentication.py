from testing.models import User
from rest_framework import authentication
from rest_framework import exceptions

from django.contrib.auth import authenticate


class Authentication(authentication.BasicAuthentication):
    def authenticate_credentials(self, userid, password, request=None):
        try:
            User.objects.get(username=userid)  # get the user
        except User.DoesNotExist:
            raise exceptions.NotFound('No such user')  # raise exception if user does not exist
        credentials = {
            'username': userid,
            'password': password
        }
        user = authenticate(request=request, **credentials)
        if user:
            return (user, None)  # authentication successful
        else:
            raise exceptions.AuthenticationFailed('Wrong Password')