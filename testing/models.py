from django.core.validators import validate_email
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):

    def save(self, *args, **kwargs):
        validate_email(self.email)
        self.username = self.email
        super(User, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return f'{self.id} - {self.username}'



class Test(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название теста')

    class Meta:
        verbose_name_plural = 'Tests'

class Question(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    number = models.IntegerField()
    text = models.TextField()

    class Meta:
        verbose_name = 'Question'
        verbose_name_plural = 'Questions'

    def __str__(self):
        return f'{self.number}'


class Character(models.Model):
    name = models.CharField(max_length=50)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Char'
        verbose_name_plural = 'Chars'

    def __str__(self):
        return f'{self.name}'

class Answer(models.Model):
    answer = models.CharField(max_length=15)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    char = models.ForeignKey(Character, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Ans'
        verbose_name_plural = 'Anses'


class Result(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    datetime = models.CharField(max_length=50)
    result = models.TextField()
