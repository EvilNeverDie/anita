from datetime import datetime

from django.shortcuts import get_object_or_404
from rest_framework import status, exceptions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from django.contrib.auth.models import AnonymousUser
from django.db import transaction
from testing.models import User, Test, Question, Character, Answer, Result
from testing.authentication import Authentication
from testing.serializers import UserCreateSerializer, UserUpdateSerializer, TestSerializer, \
    CustomUserSerializer, ResultSerializer, AllResultSerializer
import json
from django.http import JsonResponse


@api_view(['GET'])
@authentication_classes((Authentication,))
def auth(request):
    if isinstance(request.user, AnonymousUser):
        raise exceptions.PermissionDenied()
    serializer = CustomUserSerializer(instance=request.user)
    return Response(serializer.data)


class CustomUserViewSet(APIView):
    """
    retrieve:
        Детальное отображаение пользователя
    create:
        Создание пользователя
    list:
        Список пользователей
    """

    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        inst = serializer.save()
        return Response(status=status.HTTP_201_CREATED, data=CustomUserSerializer(instance=inst).data)

    def put(self, request, *args, **kwargs):
        if isinstance(request.user, AnonymousUser):
            raise exceptions.PermissionDenied()

        if request.data['password']:
            user = request.user
            user.set_password(request.data['password'])
            user.save()

            if len(request.data) > 1:
                serializer = UserUpdateSerializer(instance=request.user, data=request.data, partial=True)
                serializer.is_valid(raise_exception=True)

                serializer.save()
                return Response(serializer.data)
            else:
                return Response(UserUpdateSerializer(user))

    def get(self, request, *args, **kwargs):
        if isinstance(request.user, AnonymousUser):
            raise exceptions.PermissionDenied()
        serializer = CustomUserSerializer(instance=request.user)
        return Response(serializer.data)





@api_view(['GET'])
def get_test(request, pk):
    test = Test.objects.get(id=pk)
    serializer = TestSerializer(test)
    return Response(serializer.data)






import re

@api_view(['POST'])
def get_results(request, pk):
    # я должен получить тест
    # потом по тесту все характеристики
    # потом по каждой характеристике ответы
    # подсчитываю эту хуйню
    result = {}
    chars = Character.objects.filter(test_id=pk)
    list_answers = re.findall(r'\d+', request.data['answers'])
    for char in chars: #
        character_name = char.name
        result[character_name] = 0
        answers = Answer.objects.filter(char_id=char.id)
        for answer in answers:

            question_db = Question.objects.get(id=answer.question_id)
            answer_request = list_answers[int(question_db.number)-1]
            if answer.answer == answer_request:
                result[character_name] += 1
            elif answer_request == 2:
                result[character_name] += 0.5
    if not isinstance(request.user, AnonymousUser):
        user = request.user
        res = Result(result=str(result),
                     test_id=pk,
                     user_id=user.id,
                     datetime=str(datetime.now()))
        res.save()
        serializer = ResultSerializer(res)
    return JsonResponse(result)



@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_history(request):
    user = request.user
    results = Result.objects.filter(user_id=user.id)
    serializer = ResultSerializer(results, many=True)
    for i in serializer.data:
        i['result'] = json.loads(i['result'].replace("\'", "\""))
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAdminUser])
def get_all_history(request):
    results = Result.objects.all()
    serializer = AllResultSerializer(results, many=True)
    for i in serializer.data:
        i['result'] = json.loads(i['result'].replace("\'", "\""))
    return Response(serializer.data)



